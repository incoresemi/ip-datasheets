.. raw:: latex

   \pagebreak

Revisions
=========

**Version [1.2.0]**
  - revised pipeline working
  - improved features list
  - added configurator flow
  - improved modes description
  - adding a summary of key features in overview
  - correct non-blocking nature of execute stage

**Version [1.1.0]**

  - Added granularity constraints for PMP in feature list
  - Updated options for PMP support
  - Added Revisions section
