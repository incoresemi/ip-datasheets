#######################
Chromite Core Generator
#######################

.. .. raw:: latex
.. 
..   \begin{figure*}
..     \centering
..     \includegraphics[scale=.55]{../../source/_static/ChromiteSystem.png}
..     \caption{Chromite Processing System}
..   \end{figure*}

.. toctree::
  :glob:
  :maxdepth: 2
  :numbered:

  overview
  feature
  benchmarking
  pipeline
  modes
  interrupt
  memory
  businterface
  plic
  debugsupport
  software
  options
  revisions

.. bibliography:: refs.bib
  :all:
