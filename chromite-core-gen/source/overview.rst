Overview
========

.. _system_level:

.. figure:: _static/ChromiteSystem.png
   :width: 500px
   :height: 400px
   :align: center

   Chromite Processing System


Chromite is a fully configurable and synthesizable Core Generator capable of generating production quality RTL
based on the open `RISC-V ISA <https://riscv.org/>`_. The core generator can produce
variants of a commercial grade 5/6-stage in-order core supporting the RV[64/32]GCSU (or its subsets)
extensions of the RISC-V ISA, from the same high-level source code. Chromite leverages the high 
level abstraction offered by `Bluespec System Verilog <https://github.com/B-Lang-org>`_ to build 
highly parameterized, compact and powerful library components (like arithmetic units, branch predictors, 
caches, mmu, etc) that can be seamlessly integrated to create a solution catered to
your needs.  :numref:`system_level` shows the system level diagram of the Chromite core.

The configuration to the generator is provided through a very simple and easy to modify 
YAML file. The core generator uses a python script to generate the necessary environment variables
and macros required to generate the RTL. The generated RTL is completely synthesizable and can be 
directly integrated into conventional VLSI flows for FPGA, ASIC or verification.
:numref:`chromite_flow` shows the basic flow of the Chromite core generator.

.. _chromite_flow:

.. figure:: chromite_flow.png
   :align: center
   :width: 450px

   Chromite Core Generator Flow


The various design instances generated through Chromite can serve domains ranging from embedded 
systems, motor-control, IoT, storage, industrial applications, all the way to low-cost,
high-performance Linux based applications such as networking, gateways etc. 
The extreme parameterization of the design in conjunction with using an HLS like Bluespec, it makes 
it easy to add new features and design points on a continual basis.

A summary of key features inlcude : 

  - Support for RV[32|64]IMAFDCSUZicsr_Zifencei (compliant with the latest release of the spec)
  - Enhanced fetch micro-architecture to provide higher performance with higher code-density support
  - Highly configurable and optional Memory Management Unit
  - Configurable Multiply/Divide Units. Can be configured for higher performance or low area.
  - Floating Point Unit (FPU), an IEEE-754 compliance Floating-Point Unit with single and double
    precision support
  - Highly configurable and Latency optimized Instruction and Data Caches
  - Includes a light-weight, non-intrusive and a full-featured Debug support over JTAG
  - Configurable Hardware Breakpoints triggered by instruction/data address/content match, interrupts,
    exceptions and instruction count.
  - A fully configurable RISC-V Privileged architecture. Ability to define and control the behavior 
    of every CSR without breaking compliance to the ISA. 
  - Physical Memory Protection support as per official ISA spec to provide protection across privilege
    levels.
  - Support for virtualization mode via hypervisor (coming soon)
