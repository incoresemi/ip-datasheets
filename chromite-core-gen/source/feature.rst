
Core Level Features
===================

  * 5/6 stage in-order pipeline
  * 64/32-bit variants available
  * Configurable to support the following RISC-V ISA extensions: RV[64/32][IMAFDCSUZicsr_Zifencei]
  * Support for enhanced RISC-V privilege features:

    - Vectored interrupts and support for external interrupt controller
    - Programmable trap vector base
    - Atomic interrupt enable/disable feature support

  * Supports IEEE-754 based single and double precision Floating Point operations in hardware. 
      
    - The design of the FPU (Floating-Point-Unit) is based on Hard-float. Therefore leverages the
      berkely recoded-format for efficient implementation to support sub-normals.
    - Single unit per precision to handle fused-multiply-add, multiply, addition and subtraction
      ops. These units can be configured to meet high-frequency requirements since they are
      retime-enabled.
    - Runs at 1:1 core/FPU clock ratio.
    - A separate floating point registerfile is used to store floating point operands ( in recoded format )
    - Control the number of in-flight instructions by controlling pipeline depths of each module.

  * Integrated integer Multiply/Divide units

    - A fully pipelined multiplier. The multiplier is retime-enabled thereby allowing a 
      configurable pipeline depth at build-time.
    - The divider implements an iterative algorithm, with an early out mechanism for corner cases.

  * A flexible Co-processor interface to provide  closely-coupled integration of custom accelerators.

    - based on on the RoCC specification
    - implements a fire-wait or a fire-and-forget protocol based on the instruction dependency in
      the program order.

  * Supports AXI-4, AXI-4 Lite and TileLink bus protocols
  * Configurable L1 Caches

    * Separate instruction and data caches which can be configured with upto 4-ways and 32KiB
    * Optional ECC support for RAMs
    * Write-back with Write-allocate caches
    * Virtually indexed and physically tagged
    * Optimized to use single-ported RAMs
    * Can be disabled/enabled through software
    * Configurable replacement policies: Pseudo LRU, Random, Round-Robin
    * Data Cache has support for atomic extension operations.

  * Configurable Memory Management Unit (MMU)

    * Choose between light-weight and configurable fully-associative and set-associative TLB (Translation Lookaside Buffers) 
      architectures for Instruction and Data idependently.
    * Support for super-pages (page size more than 4KiB).
    * Translation happens in paralle with cache lookup - thereby not imposing anye extra penalty.
    * Configurable depth of fully-associative TLBs - which can enable a SW
      controlled mix of different page sizes in the same capacity.
    * Set-associative architectures provide a data-structure for each page-size (sizes of which is also
      configurable) thereby allowing a higher capacity per page-size. This choice can also enable
      better frequency closure for higher capacity
    * Hardware Page-Table-Walk (PTW) support. The page walks are cached to enable higher throughput
      on a page-miss in the TLBs.
    * The PTW is muxed between the Instruction and Data TLBS, with the latter having higher
      priority.


  * Physical Memory Protection Unit (PMP):

    * Configurable number of regions : upto 16 can be protected.
    * Can protect upto a granularity of 4 bytes for the 32-bit core and 8 bytes for the 64-bit core.
    * Certain regions can be locked for protection against machine level access as well
    
  * Branch Prediction (BPU):

    * Supports Gshare based branch prediction scheme
    * Fully-associative Branch Target Buffer with upto 64-entries
    * Variable size Branch History Buffer
    * Configurable size of Return Address Stack
    * Ability to disable predictor at runtime through software

  * Daisy Chained CSRs optimized for frequency and scaling
    
    * Allows adding of custom CSRs without having to compromise on frequency or features.
    * Can instantiate up to 32 performance counters

  * Debug support based on the RISC-V Debug spec v1.0

    - non-intrusive and light-weight implementation
    - interrupt based indication mechanism
    - Includes a program-buffer to execute custom code when halted (in debug-mode)

  * Trigger support for data and address.
  * Includes Performance counters for : 
    
    - statistics of different types of instructions based on extension
    - statistics of Caches and TLBs (like number of hits, misses, writebacks, etc)
    - statistics of traps (number of interrupts, exceptions, etc)


