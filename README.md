
**⚠ WARNING: This project has been moved to [new location](https://gitlab.incoresemi.com/globals/ip-datasheets.git). It will soon be archived and eventually deleted.**

<div class="title-block" style="text-align: center;" align="center">

# IP Datasheets
</div>

This repository contains the data-sheets of various IPs being offered by InCore Semiconductors Pvt. Ltd.

- [Chromite Core Generator](https://gitlab.com/incoresemi/ip-datasheets/-/jobs/artifacts/master/raw/incore-datasheets/chromite_coregen.pdf?job=pdf_gen): The Chromite Core is an extremely configurable and synthesizable 
  RISC-V ISA based 5-stage in-order core. The configurability of the core enables generation 
  of a range of cores, from  a tiny core targeted for power constrained IoT, to embedded and edge 
  application cores and  all the way to feature rich high-performance cores suitable for power 
  optimized linux/mobile applications
- [Interconnect IP](https://gitlab.com/incoresemi/ip-datasheets/-/jobs/artifacts/master/raw/incore-datasheets/interconnect_ip.pdf?job=pdf_gen): Variety of interconnect fabrics and bus topologies which can be used to build
  a robust SoC.
- [Azurite Core Generator](https://gitlab.com/incoresemi/ip-datasheets/-/jobs/artifacts/master/raw/incore-datasheets/azurite_coregen.pdf?job=pdf_gen): The Azurite Core is an extremely configurable and synthesizable 
  RISC-V ISA based 2-stage in-order core. The configurability of the core enables generation 
  of a range of cores, from  a tiny core targeted for domains ranging from sensor fusion, power 
  managment, motor control, Low-power MCUs, health wearable monitors, smart IoT, connected toys, etc.

