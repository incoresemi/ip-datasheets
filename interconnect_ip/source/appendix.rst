.. _appendix:

###########
Appendix
###########

* `Open Bluespec Compiler <https://github.com/B-Lang-org/bsc>`__ can be cloned and installed by following the steps in the repository. 
* For design simulation, please install `Verilator <https://www.veripool.org/projects/verilator/wiki/Installing>`__
* InCore python utilities require Python 3.7.0. Detailed instructions for the same is provided below.


Install Python Dependencies
---------------------------

All python utilities require ``pip`` and ``python`` (>=3.7) to be available on your system. If you have issues installing, either of these, directly on your system we suggest using a virtual environment like `pyenv` to make things easy.


First Install the required libraries/dependencies:

.. code-block:: bash

    $ sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
        libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
        xz-utils tk-dev libffi-dev liblzma-dev python-openssl git

Next, install `pyenv`

.. code-block:: bash

  $ curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

Add the following to your `.bashrc` with appropriate changes to username:

.. code-block:: bash

  export PATH="/home/<username>/.pyenv/bin:$PATH"
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"

Open a new terminal and create a new python virtual environment:

.. code-block:: bash

  $ pyenv install 3.7.0
  $ pyenv virtualenv 3.7.0 myenv

Now you can activate this environment in any other terminal :

.. code-block:: bash

  $ pyenv activate myenv
  $ python --version

Project specific packages can be installed as below:

.. code-block:: bash

  $ pip install cogapp
