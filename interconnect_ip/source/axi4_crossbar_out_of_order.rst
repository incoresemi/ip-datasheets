.. _axi4_crossbar:

##################################################################
AXI4 Cross-bar Interconnect with read re-ordering and interleaving
##################################################################

The AXI4 Cross-bar interconnect is used to connect one or more AXI4 compliant master devices
to one or more AXI4 compliant slave devices. It includes the following features:

- ID width can range upto 32-bits
- The address widths can go upto 64-bits.
- The data widths supported are: 32, 64, 128, 256, 512 and 1024.
- Provides a configurable size of user-space on each channel.
- Supports aligned and unaligned transfers.
- Supports read-only and write-only master-slave combinations resulting in reduced overheads.
- Supports static and round robin priority arbitration amongst masters
- Supports read reodering and interleaving

**AXI4 Interconnect Limitations**
  - AXI4 Quality of Service (QoS) signals do not influence arbitration priority within the crossbar
    and the signals are simply propagated without any manipulation from master to slaves.
  - The cross bar will not time-out if any destination of the cross-bar stalls indefinitely.
  - Low power interface features are not currently supported.

Henceforth, ``M`` represents the number of master-devices 
and ``S`` would represent the number of slave-devices connected to the
cross-bar.

.. _axi4_parameters:

Parameters
==========

Each instance of the cross-bar consists of vectored AXI4 slave signals which should be
connected to either master-transactors or directly to a master interface and
vectored AXI4 master signals which should be connected to either slave-transactors or
directly to a slave interface.

The cross-bar interfaces are parameterized with the following parameters:

.. tabularcolumns:: |l|L|

.. _axi4_crossbar_params:

.. table:: AXI4 Cross-bar Interface Parameters

  ======================  ================
  Parameter Name          Description
  ----------------------  ----------------
  ``wd_id``               size of the id fields in all the channels.
  ``wd_addr``             size of the address fields in the read-address and write-address channels
  ``wd_data``             size of the data fields in the read-response and write-data channels
  ``wd_user``             size of the user fields in all the channels.
  ``tn_num_masters``      indicates the number of masters that will be connected to
                          this fabric.
  ``tn_num_slaves``       indicates the number of slaves that will be connected to
                          this fabric.
  ``tn_num_slaves_bits``  indicates the number of bits that will be used to represent total slaves
  ``seq_count_width``     size of sequence count 
  ``acceptance_width``    value of this represents the number of live transaction master keeps
                          tracks of for ``M`` --> ``S`` read transaction
  ``issuing_width``       value of this represents the number of live transaction slaves keeps             
                          track of for ``M`` --> ``S`` read transaction  
  ======================  ================

While the above parameters control the interface signals of the cross-bar, the
following arguments need to be provided to the module instance to control the arbitration
and connection:

.. tabularcolumns:: |l|L|

.. _axi4xbar_table_modargs:

.. table:: AXI4 Cross-bar Module Arguments

  ===================== =============================================================================
  Parameter Name        Description
  --------------------- -----------------------------------------------------------------------------
  ``fn_rd_memory_map``  A function that provides a memory map of the address-space for the read
                        channel. It takes an address as an argument and returns a slave-number. 
  ``fn_wr_memory_map``  A function that provides a memory map of the address-space for the write. 
                        channel. It takes an address as an argument and returns a slave-number. 
  ``read_slave``        A mask vector of size ``S`` that indicates if a particular slave has read support
                        or not.
  ``write_slave``       A mask vector of size ``S`` that indicates if a particular slave has write support
                        or not.
  ``fixed_priority_rd`` A vector of size ``M`` that indicates if the respective master has fixed
                        priority or participates in round-robin arbitration on the read channel. 
                        Setting a bit to 1 indicates that the master has fixed priority, while setting it 
                        to 0 implies that it participates in round-robin arbitration with other masters 
                        who have their bits set to 0.
  ``fixed_priority_wr`` A vector of size``M`` that indicates if the respective master has fixed
                        priority or participates in round-robin arbitration on the write channel. 
                        Setting a bit to 1 indicates that the master has fixed priority, while setting it 
                        to 0 implies that it participates in round-robin arbitration with other masters 
                        who have their bits set to 0.
  ===================== =============================================================================


.. tip:: By using the above two functions, one can generate an area optimized cross-bar for a given SoC 
  which may contain read-only and write-only slaves.

.. note:: It is recommended to keep the masters participating in round-robin to be contiguous
   (either at the LSBs or the MSBs) for consistent fairness. More details on arbitration policy are
   available in :ref:`arbitration_axi4`

Theory of Operation
===================

Following is the convention/glossary of terms that are used in the following sections:

  * **AR Channel**: refers to the read address channel of the AXI4 protocol
  * **AW Channel**: refers to the write address channel of the AXI4 protocol
  * **W  Channel**: refers to the write data channel of the AXI4 protocol
  * **R  Channel**: refers to the read data channel of the AXI4 protocol
  * **B  Channel**: refers to the write response channel of the AXI4 protocol

.. _axi4xbar_masterslave_ids:

Master/Slave IDs
----------------

As per the AXI4 standard, it is recommended that when a master is connected to an interconnect, 
the interconnect appends additional bits to the *ARID*, *AWID* and *WID* fields that are unique to that 
master device, as this simplifies routing of slave responses.

However, in this implementation we do not append any such
bits to the ID fields as will be clear from :numref:`axi4xbar_routing_transactions`. The implementation
does assign unique ids to each master and slave device based on the port to which the
master/slave device is connected to, which are used for routing purposes.

.. _axi4xbar_transactors:

Transactors
-----------

.. _axi4xbar_img_transactors:

.. figure:: _static/transactors.png
   :align: center

   Master and Slave Transactors used within the AXI4 Crossbar

The cross-bar internally instantiates ``M`` slave transactors and ``S``
master transactors. These transactors provide an AXI4 interface on one side (driving the external
signals of the cross-bar) and a FIFO like interface on the other side. 
The ``M`` slave transactors provide an AXI4
slave interface externally which are to be connected to ``M`` master devices like DMA masters, cache
masters, etc. The ``S`` master transactors provide an AXI4 master interface externally, which are to
be connected to slave devices like, UART, SPI, Memory controllers, etc. 

Each of these transactors internally include a two entry FIFO on each of the five AXI4
channels. For the slave transactors, the FIFOs on the *AW*, *W* and *AR* 
channels are enqueued when the corresponding master devices drive valid transactions on these
channels. We will refer to these set of FIFOs as **axi-side** FIFOs.
The FIFOs on the *R* and *B* channels are enqued by logic within the
interconnect and will be refered to as **xbar-side** FIFOs. The axi-side and xbar-side fifos 
for a master transactor are interchanged as compared to the slave transactor. 
:numref:`axi4xbar_img_transactors` shows the interface signals and the placement of these FIFOs for the 
master and slave trasactors.

When a master device initiates a new trasaction on the *AR*, *AW* or *W* channels the corresponding axi-side
FIFOs hold a valid entry for routing within the crossbar interconnect in the immediate cycle. 
Similarly, when the  crossbar routes an *R* or *B* packet to the corresponding xbar-side FIFOs of the
transactor, the master device will see these transactions in the immediate cycle.
:numref:`axi4xbar_rd_addr_wave` shows the behavior in case of the *AR* channel being driven by a master device,
and the corresponding axi-side FIFO in the connected slave transactor. 


.. _axi4xbar_rd_addr_wave:

.. wavedrom::
  :align: center
  :caption: Bevahvior of FIFO on the read-address channel of the slave transactor

  {"signal": [
    {"name": "CLK", "wave": "P....."},
    {"name": "ARVALID", "wave": "0.1.0."},
    {"name": "ARADDR",  "wave": "x.==x.", "data": ["A1", "A2"] },
    {"name": "ARREADY",  "wave": "1...0."},
    {"name": ".."},
    {"name": ".."},
    {"name": "s-ar-fifo.notEmpty", "wave": "0..1.."},
    {"name": "s-ar-fifo.notFull",  "wave": "1...0."}
  ]}

Rest of the FIFOs in the slave transactor work analogously to the above behavior. 
The next section will discuss how transactions received on the axi-side FIFOs are
routed to their destination through the crossbar.

.. _axi4xbar_routing_transactions:

Transaction Routing
-------------------

.. _axi4xbar_img_crossbar:

.. figure:: _static/crossbar3.png
   :align: center
   :height: 700px

   Crossbar interconnect with transactors and master/slave devices

Once the transactions from the master/slave devices are latched into the corresponding axi-side
FIFOs, they need to be routed through the crossbar to their target nodes. It should be noted here
that the routing logic for the read bus (comprising of the *AR* and *R* channels/packets) is separate
from the routing logic for the write bus (comprising of the *AW*, *W* and *B* channels/packets) to
leverage maximum performance. Before proceding to understand how the crossbar routes the transaction 
supporting read ordering and interleaving next section shed some light on what is reordering and 
interleaving and what we are expect from crossbar when we say it supports reordering ans interleaving

Re-ordering:
  Re-ordering in axi4 is concept that helps optimize the transaction between master and slave. 
  According to it the device should be able to adhere to the intended order even after recieving the transaction 
  in an out of order manner.
  
  Let's setup an example to understand the concept implementation here in axi4.
  Let say you have one ``M`` and two ``S`` devices and one of the slave device is slow compared to 
  other one i.e. it is able to serve the response to master only after some time whereas other slave
  is fast and serves the response as soons as it recieve any kind of request. Now the master has issued
  requested the slower slave first and then the other slave for some kind of request, in the absence of 
  reordering the master should revieve the responses from the slower slave first and then the other 
  slave as to ensure the order in which the requests were send, but in case where implementation supports
  reordering the master can recieve the responses in an out of order fashion allowing the master
  to process the response in faster fashion and not wait for the response from the slowest in a chain
  of requests.

Interleaving:
  Interleaving in axi4 is only present in read channels, this also helps in optimizing the transaction
  between master and slave. According to it multiple devices can interleave there responses if they
  are ready at the same time, they need not to wait for other devices to finish there chain of responses.

  Let's setup an scenario to understand this feature in action, lets say ``slave0`` has to send
  ``5`` response to master and ``slave1`` has to send ``3`` responses, in the absence of interleaving
  ``slave0`` first need to send all the ``5`` responses and then ``slave1`` will get turn to send it's 
  ``3`` responses or in opposite order, but in case of interleaving slaves can interleave there 
  responses.  

The next paragraphs will discuss the routing for read and write transaction, before that keep in mind 
only read channels  (comprising of the *AR* and *R*) supports both re-ordering and interleaving, the 
interleaving feature in the write channel was removed from *AXI4* and was present till *AXI3*
this removal and specs constraint that the first write data request cannot be reorder i.e. if there 
are two request then the first write data transaction should corresponnd to first write address and then 
second write data request should correspond to the second write address transaction, made the write
channels (comprising of the *AW*, *W* and *B*) devoid of the interleaving and reordering feature.

Read Transactions:
  Once the *arfifo* in the slave transactors have a valid entry, the target master transactor to which
  this transaction needs to be routed is deduced using the ``fn_rd_memory_map`` function provided to
  the design at compile time. However, the salve transactor can only carry out the transfer to the
  master transactor, if the *arfifo* in the master transactor has atleast one empty slot. 
  
  It is possible that the slave device connected to the master transactor is busy has not been able
  to service the pending request(s) that are present in *arfifo* thereby causing it to become full in
  due course of time.
  In such a scenario, a new request from a slave
  transactor can no longer be enqued to that particular slave device connected to the master transactor
  while wating for that slave device to service the pending request and free up the space slave transactor
  will look for any other potential service request that needs to be enqued to the master transactor of
  other slave device that are able to take in service request. As soon as there is free space in the
  particular slave device that became full of request the slave transactor will enque any pending service
  request to master transactor of that slave device.   

  .. note:: Transactions sitting in any of the fifos in the transactors, like the case above, are
     defined as in-flight transactions, which have been received by the master/slave device but have
     not yet reached the target slave/master device.

  To keep track of the pending transactions generated by a slave transactor and where the response
  of master transactor should be routed to, the crossbar maintains a series of **route-info** register
  in the slave and master transactors. For the read transactions, each slave transactor has **route-info**
  and **sequence-count-per-id** register, **route-info** register are of size equal to the ``acceptance width`` 
  specified by the list of parameter the user specify at the time of code compiling, each entry of route-info
  register is defined as a user defined data type ``Map_s_to_m`` which consist of two fields ``transaction id`` 
  and ``sequence id``.

  .. note:: The transaction id mention here is same as the arid field in the address read channel,
    and for multiple transaction with same arid and same master and slave address ``sequence id`` is 
    used to make a distinction between those transactions. 

  .. note:: The sequence id will be same for all the transaction of all the beats associated with a 
    single brust transaction.

  The size of **sequence-count-per-id** is also same as ``acceptance width`` and each entry in the register
  keeps track of the count of sequence id where the maximum count value it can hold is defined by the ``seq_count_width`` 
  parameter i.e it tells how many bits can sequence id count be represented in. The sequence-count-per-id 
  register keep tracks of the total number of transaction that are issued a particular arid, which basically
  helps the master side to see keep track of the number of responses it needs to recieve for that particular arid

  The sequence count increases for each transaction the slave transactor issues for a particular arid,
  the count goes to the value of (maximum_count - 2) and then wraps around i.e if the maximum count
  value is 16 then the count will go from 0 -> 14 then 0, 1, 2 ... 

  In the slave side the **route-info** register have a similar but different structure the size of the 
  route-info register is equal to the ``issuing width`` the user specifies as a parameter in the code,
  and each entry of the route-info register is of user defined data type ``Map_m_to_s`` which contains 
  4 fields ``master number``, ``slave number``, ``transactio id`` and ``sequence id``.

  For each read transaction first it is checked if the ``arid`` is in flight, if the condition is true
  then value corresponding to that particular arid is incermented in the  sequence-count-per-id register,
  else if the condition is false new entry is there to the route-info register of both concerned master and
  slave transactor, with a default values and with appropriated value of master id (response is expected
  routed back) and slave id(indicating the correct master transactor) in the route-info register of 
  master transactor side 

  .. note:: This implementation has a support for interleaving and reordering, in the read channel
     thus slave-devices connected to this crossbar can have a re-ordering depth of more than
     1. i.e. it allows responses from the particular slave device be in out of order compared to the 
     the requests presented at its port.


  The response from master transactor details is cross verified with the route-info details of both 
  master and slave transactor, the master transactor has the details of which slave transactor it need to send the
  response to and the concerned slave transactor keeps track of the sequence id of which the response should 
  correspond to, this sequence id matching allow the response to be interleaved as the slave transactor will
  only accept the transaction when there is match. After the master recieve the response the route-info delatils
  is updated with the sequence id the slave transactor is expecting.

  Once the master transactor sends an *RLAST* signal on the *R* packet,or the maximum sequence count is
  reached the corresponding route-info register are cleared allowing the slave/master next transaction 
  details to used the cleared space.

  :numref:`axi4xbar_rd_routing` shows the above behavior of the read transaction routing happening
  between a master device with ID M1 and a slave device with ID S1.


  .. _axi4xbar_rd_routing:
  
  .. wavedrom::
    :align: center
    :caption: Read transaction routing through the crossbar.
  
    {"signal": [
        {"name": "CLK",     				  "wave": "P......"},
      [ "Master Device",
        {"name": "M-ARADDR",  				"wave": "x.=x...", "data": ["A1", "A2"] },
        {"name": "M-ARVALID", 				"wave": "0.10..."}
      ],
      	{},
      ["Slave Transactor",
      	{"name": "s-ar-fifo.enq",			"wave": "0.10..."},
      	{"name": "s-ar-fifo.deq",			"wave": "0..10.."},
        {"name": "s-ar-fifo.notEmpty","wave": "0..10.."},
       	{"name": "s-route-info",			"wave": "x...=x.", "data": ["S1", "A2"] }
      ],
      	{},
      ["Master Transactor",
      	{"name": "m-ar-fifo.enq",			"wave": "0..10.."},
      	{"name": "m-ar-fifo.deq",			"wave": "0...10."},
        {"name": "m-ar-fifo.notEmpty","wave": "0...10."},
       	{"name": "m-route-info",			"wave": "x...=x.", "data": ["M1", "A2"] }
      ],
        {},
      ["Slave Device",
    
        {"name": "S-ARADDR",  				"wave": "x...=x.", "data": ["A1", "A2"] },
        {"name": "S-ARVALID", 				"wave": "0...10."}
      ],
      {}
    ],
      
      "head":{"tick":0}
    }

Write Transactions:
  The working of the write transactions is done by a set of route-info FIFOs that are maintained on each master and slave transactor
  for the write channel i.e. each transactor maintains a route-info FIFO for the *AW* channel and another route-info for the *W* channel.
  To keep track of the (current and pending) transactions generated by a slave transactor and where the response of
  master transactor should be routed to, the crossbar maintains a series of route-info FIFOs in the 
  slave and master transactors. For the write address transactions, each slave and master transactor include 
  an 8-entry route-info FIFO to store the master and slave ids participating in a transaction. In a 
  slave transactor, this FIFO indicates which slave-device’s (master transactor) response is expected
  to be routed to the connected master-device. Similarly, the route-info FIFO in a master transactor
  indicates the ID of the master-device (slave transactor) to whom the response is to be routed to. 

  Therefore, when a valid transfer occurs between the aw-fifo of
  the slave transactor and the aw-fifo of the master transactor, the *AW* and *W* channel route-info
  FIFOs of both the transactors are updated. Ths *W* channel route-info FIFOs, ensure that the
  sub-sequent beats of the transaction from the slave transactor are routed to the correct master
  transactor.

  The route-info FIFOs are popped/dequeued when the write response of the *B* channel is received.
  :numref:`axi4xbar_wr_routing` shows the behavior of the write transactions through the crossbar.

  .. _axi4xbar_wr_routing:
  
  .. wavedrom::
    :align: center
    :caption: Write transaction routing through the crossbar.
    :height: 700px
  
    {"signal": [
        {"name": "CLK",     				  "wave": "P......"},
      [ "Master Device",
        {"name": "M-AWADDR",  				"wave": "x.=x...", "data": ["A1", "A2"] },
        {"name": "M-AWVALID", 				"wave": "0.10..."},
        {"name": "M-WDATA",    				"wave": "x.=x...", "data": ["D1", "A2"] },
        {"name": "M-WVALID", 	  			"wave": "0.10..."}
      ],
      	{},
      ["Slave Transactor",
      	{"name": "s-aw-fifo.enq",			"wave": "0.10..."},
      	{"name": "s-aw-fifo.deq",			"wave": "0..10.."},
        {"name": "s-aw-fifo.notEmpty","wave": "0..10.."},
      	{"name": "s-w-fifo.enq",			"wave": "0.10..."},
      	{"name": "s-w-fifo.deq",			"wave": "0..10.."},
        {"name": "s-w-fifo.notEmpty", "wave": "0..10.."},
       	{"name": "s-route-info-aw",  	"wave": "x...=x.", "data": ["S1", "A2"] },
       	{"name": "s-route-info-w",  	"wave": "x...=x.", "data": ["S1", "A2"] }
      ],
      	{},
      ["Master Transactor",
      	{"name": "m-aw-fifo.enq",			"wave": "0..10.."},
      	{"name": "m-aw-fifo.deq",			"wave": "0...10."},
        {"name": "m-aw-fifo.notEmpty","wave": "0...10."},
      	{"name": "m-w-fifo.enq",			"wave": "0..10.."},
      	{"name": "m-w-fifo.deq",			"wave": "0...10."},
        {"name": "m-w-fifo.notEmpty", "wave": "0...10."},
       	{"name": "m-route-info-aw",		"wave": "x...=x.", "data": ["M1", "A2"] },
       	{"name": "m-route-info-w",		"wave": "x...=x.", "data": ["M1", "A2"] }
      ],
        {},
      ["Slave Device",
    
        {"name": "S-AWADDR",  				"wave": "x...=x.", "data": ["A1", "A2"] },
        {"name": "S-AWVALID", 				"wave": "0...10."},
        {"name": "S-WDATA",    				"wave": "x...=x.", "data": ["D1", "A2"] },
        {"name": "S-WVALID", 		  		"wave": "0...10."}
      ],
      {}
    ],
      
      "head":{"tick":0}
    }

.. _arbitration_axi4:

Arbitration Policy
------------------

When multiple slave transactors select the same master-transactor to perform a similar type of
(read/write) transaction, arbitration is required to choose which slave transactor will succeed.
The implementation supports two types of arbitration policy: fixed and round-robin. At design time,
the user needs to define which slave-transactors will participate in fixed arbitration and which will
participate in round-robin arbitration using the ``fixed_priority_*`` parameters defined in
:numref:`axi4xbar_table_modargs`. 

.. note:: Since the read and write channels operate independently, it
  is possible to have the read port of a master-device to have a fixed priority while the write port
  can participate in round-robin arbitration or vice-versa.

Fixed Priority Arbitration:
  By default, the arbitration is granted based on the relative priority of the associated IDs of the
  slave transactors (refer to :numref:`axi4xbar_masterslave_ids` for more details on how IDs are
  assigned). A slave transactor with a lower ID has higher priority over a slave transactor with a
  higher ID. 

Round-Robin Arbitration:
  For round robin arbitration, the design maintains separate *select* registers for read and write port
  arbitration, whose reset value is 0. The select register indicates the threshold ID value. 
  When a contention occurs, the participating slave transactor whose ID is
  immediately above the select register wins the arbitration. Once a slave transactor is chosen, the
  select register is updated by a value one greater than the ID of the winning transactor. In case
  the winning ID is the highest then the value assigned it 0.

  It is possible, that any point there is a contention amongst slave transactors with fixed
  arbitration and slave transactors configured for round-robin arbitration. In such a case, 
  the slave transactor with fixed arbitration having an ID lower than the ID of the slave
  transactor winning the round-robin arbitration is given access to the master transactor. If all the slave
  transactors with fixed arbitration have an ID higer than the slave transactor winning the
  round-robin arbitration, then the latter is given access to the master transactor.

  :numref:`axi4xbar_rr_arbitration` Shows how round robin arbitration would work for a crossbar
  consisting of 2 slave transactors with a select threshold of 1.

.. _axi4xbar_rr_arbitration:

.. wavedrom::
  :caption: Round Robin arbitration amongst 2 masters
  :align: center

  {"signal": [
      {"name": "CLK",     				"wave": "P......."},
    	{"name": "RD-SELECT",  				"wave": "=.......", "data": ["1"] },
    ["Master-1",
    	{"name": "M1-ARADDR",  				"wave": "x.=x....", "data": ["A1"] },
      {"name": "M1-ARVALID", 				"wave": "0.10...."}
    ],
     {},
    ["Master-2",
    	{"name": "M2-ARADDR",  				"wave": "x.=x....", "data": ["A2"] },
      {"name": "M2-ARVALID", 				"wave": "0.10...."}
    ],
    	{},
      {},
    ["Slave Xactor-1",
      {"name": "s1-ar-fifo.NotEmpty", 	"wave": "0..1.0.."},
     	{"name": "s1-route-fifo",			"wave": "x....=x.", "data": ["S1"] }
    ],
     {},
     {},
    ["Slave Xactor-2",
      {"name": "s2-ar-fifo.NotEmpty", 	"wave": "0..10..."},
     	{"name": "s2-route-fifo",			"wave": "x...=x..", "data": ["S1"] }
    ],
     {},
     {},
    ["Master Xactor",
      {"name": "m-ar-fifo.NotEmpty", 		"wave": "0...1.0."},
     	{"name": "m-route-fifo",			"wave": "x...==x.", "data": ["M2", "M1"] }
    ],
      {},
      {},
    ["Slave Device",
  
      {"name": "S-ARADDR",  				"wave": "x...==x.", "data": ["A2", "A1"] },
      {"name": "S-ARVALID", 				"wave": "0...1.0."}
    ],
      {}
  ],
    
    "head":{"tick":0}
  }

.. _address_decode_axi4xbar:

Address Decode
--------------

The cross-bar module requires two functions (``fn_rd_memory_map`` and
``fn_wr_memory_map``) to be
provided at design time which are used by the read and write channels to
identify a target slave-device. The functions should take as input an address of the
same width : ``wd_addr`` and return a slave-device id number which indicates which one of
the vectored slave interfaces have been selected for this transaction by the master device. 

Disjoint address spaces selecting the same slave are also allowed. The
distinction between these address spaces is the responsibility of the slave-device.

.. note:: If a device is read-only or write-only then its memory map allocation can be skipped
  from the ``fn_rd_memory_map`` or ``fn_wr_memory_map`` functions respectively to remove the
  corresponding channel connections.


Error signaling
---------------

The cross-bar does not internally generate the DECERR, but expects that one of
the ``S`` slaves is an **Error Slave** which is selected for all holes within
the address maps (applies to both read and write channels) and responds with a DECERR.

.. tip:: When defining the ``fn_wr_memory_map`` and ``fn_rd_memory_map`` functions, its recommended to
   assign the error slave ID under last *else* condition of an if-else construct or under the 
   *default* condition of a case statement.


Instance Mapping in Verilog
---------------------------

The following provides a mapping between the data structures/elements mentioned above to 
instances/signals available in the generated Verilog. 

.. note:: The following mapping assumes the instance name of the crossbar to be *fabric*.

Route-Info FIFOs: 
  The route-info FIFO instances in the slave transactors are named: 

    - ``fabric_f_s_rd_route_info_<num>``
    - ``fabric_f_s_wd_route_info_<num>``
    - ``fabric_f_s_wr_route_info_<num>``

  The route-info FIFO instances in the master transactors are named: 

    - ``fabric_f_m_rd_route_info_<num>``
    - ``fabric_f_m_wd_route_info_<num>``
    - ``fabric_f_m_wr_route_info_<num>``
  
  The sequence count per id register instance in the fabric

    - ``fabric_rg_seq_count_per_arid<num>``

  *<num>* in the above strings should be replaced by the ID of the slave/master transactor assigned at
  design time.

Channel FIFOs:
  The various xbar-side and axi-side FIFOs within the slave transactors are named:

    - ``fabric_xactors_from_masters_<num>_f_arfifo``
    - ``fabric_xactors_from_masters_<num>_f_awfifo``
    - ``fabric_xactors_from_masters_<num>_f_wfifo``
    - ``fabric_xactors_from_masters_<num>_f_rfifo``
    - ``fabric_xactors_from_masters_<num>_f_bfifo``
  
  The various xbar-side and axi-side FIFOs within the master transactors are named:

    - ``fabric_xactors_to_slaves_<num>_f_arfifo``
    - ``fabric_xactors_to_slaves_<num>_f_awfifo``
    - ``fabric_xactors_to_slaves_<num>_f_wfifo``
    - ``fabric_xactors_to_slaves_<num>_f_rfifo``
    - ``fabric_xactors_to_slaves_<num>_f_bfifo``
  
  *<num>* in the above strings should be replaced by the ID of the slave/master transactor assigned at
  design time.

Using the Cross-bar IP
======================

The IP is designed in BSV and available at: https://gitlab.com/incoresemi/blocks/fabrics .
The following steps demonstrate on how to configure and generate verilog RTL of
the cross-bar IP. 

.. note:: The user is expected to have the downloaded and installed 
  open-source bluespec compiler available at: https://github.com/BSVLang/Main

Configuration and Generation
----------------------------

1. **Setup**:

   The IP uses the python based `cogapp tool <https://nedbatchelder.com/code/cog/>`_ to generate bsv files with cofigured instances. 
   Steps to install the required tools to generate the configured IP in verilog RTL can be found 
   in `Appendix <appendix.html>`_. 
   If you are using a python virtual environment make sure its activated before proceeding to 
   the following steps.

2. **Clone the repo**:

   .. code:: bash
   
      git clone https://gitlab.com/incoresemi/blocks/fabrics.git
      ./manager.sh update_deps
      cd axi4/test

3. **Configure Design**: 
   
   The yaml file: ``axi4_crossbar_out_of_order_config.yaml`` 
   is used for configuring the crossbar. Please refer to :numref:`axi4_crossbar_params` 
   for information on the parameters used in the yaml file. 
   
   Address map should also be specified in this file using the slot-number 
   as the key of the dictionary. Following rules apply to the memory map:

     1. Slot-numbering should be from 0 to ``tn_num_slaves - 1``
     2. Each slave can have one of the following access policies: ``read-only``, ``write-only``, 
        ``read-write`` and ``error``. An ``error`` slave need not have the ``base`` and ``bound``
        fields specified.
     3. Atleast one of the slaves should have access as ``error``
     4. While providing the address based and bounds, remember the base is included and bound is not
        for the device under consideration


4. **Generate Verilog**: Use the following command with required settings to
   generate verilog for synthesis/simulation:

   .. code:: bash

     make TOP_FILE=axi4_crossbar_out_of_order.bsv TOP_MODULE=mkaxi4_crossbar_out_of_order generate_instances
   
   The generated verilog file is available in: ``build/hw/verilog/mkaxi4_crossbar_out_of_order.v``

5. **Interface signals**: In the generated verilog, the vectored slave interface
   signals (to which masters will be connected to) are prefixed with
   ``frm_master_<num>``. The vectored master interface signals (to which slaves
   will be connected to) are prefixed with ``to_slaves_<num>``. Since the IP is a
   synchronous IP, the same clock and reset (active-low) signals (``ACLK`` and ``ARESETN``) are used by 
   all channles across all devices.

6. **Simulation**: The top module for simulation is ``mkaxi4_crossbar_out_of_order``. Please follow the steps
   mentioned in :numref:`verilog_sim_env` when compiling the top-module for simulation

Verilog Signals
---------------

:numref:`verilog_names_axi4` describes the signals in the generated verilog for the following configuration 

.. code:: yaml

    wd_id: 4
    wd_addr: 32
    wd_data: 64
    wd_user: 0
    tn_num_masters: 1
    tn_num_slaves:  1
    fixed_priority_rd: 0b1
    fixed_priority_wr: 0b1
    tn_num_slaves_bits: TLog #(tn_num_slaves)
    seq_count_width: 4
    acceptance_width: 4
    issuing_width: 4

    memory_map:
      0:
        access: error


.. _verilog_names_axi4:

.. table:: AXI4 cross-bar interface signals in from verilog

  ==============================  =========  ==========  ============================ 
  Signal Names                    Direction  Size(Bits)  Description          
  ------------------------------  ---------  ----------  ----------------------------
  ACLK                            Input      1           clock for all channels 
  ARESETN                         Input      1           an active low reset    
  frm\_master\_0\_AWREADY         Output     1           signal sent to master-device
  frm\_master\_0\_WREADY          Output     1           signal sent to master-device
  frm\_master\_0\_BVALID          Output     1           signal sent to master-device
  frm\_master\_0\_BID             Output     4           signal sent to master-device
  frm\_master\_0\_BRESP           Output     2           signal sent to master-device
  frm\_master\_0\_ARREADY         Output     1           signal sent to master-device
  frm\_master\_0\_RVALID          Output     1           signal sent to master-device
  frm\_master\_0\_RID             Output     4           signal sent to master-device
  frm\_master\_0\_RDATA           Output     64          signal sent to master-device
  frm\_master\_0\_RRESP           Output     2           signal sent to master-device
  frm\_master\_0\_RLAST           Output     1           signal sent to master-device
  to\_slave\_0\_AWVALID           Output     1           signal sent to slave-device 
  to\_slave\_0\_AWID              Output     4           signal sent to slave-device 
  to\_slave\_0\_AWADDR            Output     32          signal sent to slave-device 
  to\_slave\_0\_AWLEN             Output     8           signal sent to slave-device 
  to\_slave\_0\_AWSIZE            Output     3           signal sent to slave-device 
  to\_slave\_0\_AWBURST           Output     2           signal sent to slave-device 
  to\_slave\_0\_AWLOCK            Output     1           signal sent to slave-device 
  to\_slave\_0\_AWCACHE           Output     4           signal sent to slave-device 
  to\_slave\_0\_AWPROT            Output     3           signal sent to slave-device 
  to\_slave\_0\_AWQOS             Output     4           signal sent to slave-device 
  to\_slave\_0\_AWREGION          Output     4           signal sent to slave-device 
  to\_slave\_0\_WVALID            Output     1           signal sent to slave-device 
  to\_slave\_0\_WDATA             Output     64          signal sent to slave-device 
  to\_slave\_0\_WSTRB             Output     8           signal sent to slave-device 
  to\_slave\_0\_WLAST             Output     1           signal sent to slave-device 
  to\_slave\_0\_BREADY            Output     1           signal sent to slave-device 
  to\_slave\_0\_ARVALID           Output     1           signal sent to slave-device 
  to\_slave\_0\_ARID              Output     4           signal sent to slave-device 
  to\_slave\_0\_ARADDR            Output     32          signal sent to slave-device 
  to\_slave\_0\_ARLEN             Output     8           signal sent to slave-device 
  to\_slave\_0\_ARSIZE            Output     3           signal sent to slave-device 
  to\_slave\_0\_ARBURST           Output     2           signal sent to slave-device 
  to\_slave\_0\_ARLOCK            Output     1           signal sent to slave-device 
  to\_slave\_0\_ARCACHE           Output     4           signal sent to slave-device 
  to\_slave\_0\_ARPROT            Output     3           signal sent to slave-device 
  to\_slave\_0\_ARQOS             Output     4           signal sent to slave-device 
  to\_slave\_0\_ARREGION          Output     4           signal sent to slave-device 
  to\_slave\_0\_RREADY            Output     1           signal sent to slave-device 
  frm\_master\_0\_AWVALID         Input      1           signal driven by master-device
  frm\_master\_0\_AWID            Input      4           signal driven by master-device
  frm\_master\_0\_AWADDR          Input      32          signal driven by master-device
  frm\_master\_0\_AWLEN           Input      8           signal driven by master-device
  frm\_master\_0\_AWSIZE          Input      3           signal driven by master-device
  frm\_master\_0\_AWBURST         Input      2           signal driven by master-device
  frm\_master\_0\_AWLOCK          Input      1           signal driven by master-device
  frm\_master\_0\_AWCACHE         Input      4           signal driven by master-device
  frm\_master\_0\_AWPROT          Input      3           signal driven by master-device
  frm\_master\_0\_AWQOS           Input      4           signal driven by master-device
  frm\_master\_0\_AWREGION        Input      4           signal driven by master-device
  frm\_master\_0\_WVALID          Input      1           signal driven by master-device
  frm\_master\_0\_WDATA           Input      64          signal driven by master-device
  frm\_master\_0\_WSTRB           Input      8           signal driven by master-device
  frm\_master\_0\_WLAST           Input      1           signal driven by master-device
  frm\_master\_0\_BREADY          Input      1           signal driven by master-device
  frm\_master\_0\_ARVALID         Input      1           signal driven by master-device
  frm\_master\_0\_ARID            Input      4           signal driven by master-device
  frm\_master\_0\_ARADDR          Input      32          signal driven by master-device
  frm\_master\_0\_ARLEN           Input      8           signal driven by master-device
  frm\_master\_0\_ARSIZE          Input      3           signal driven by master-device
  frm\_master\_0\_ARBURST         Input      2           signal driven by master-device
  frm\_master\_0\_ARLOCK          Input      1           signal driven by master-device
  frm\_master\_0\_ARCACHE         Input      4           signal driven by master-device
  frm\_master\_0\_ARPROT          Input      3           signal driven by master-device
  frm\_master\_0\_ARQOS           Input      4           signal driven by master-device
  frm\_master\_0\_ARREGION        Input      4           signal driven by master-device
  frm\_master\_0\_RREADY          Input      1           signal driven by master-device
  to\_slave\_0\_AWREADY           Input      1           signal driven by slave-device 
  to\_slave\_0\_WREADY            Input      1           signal driven by slave-device 
  to\_slave\_0\_BVALID            Input      1           signal driven by slave-device 
  to\_slave\_0\_BID               Input      4           signal driven by slave-device 
  to\_slave\_0\_BRESP             Input      2           signal driven by slave-device 
  to\_slave\_0\_ARREADY           Input      1           signal driven by slave-device 
  to\_slave\_0\_RVALID            Input      1           signal driven by slave-device 
  to\_slave\_0\_RID               Input      4           signal driven by slave-device 
  to\_slave\_0\_RDATA             Input      64          signal driven by slave-device 
  to\_slave\_0\_RRESP             Input      2           signal driven by slave-device 
  to\_slave\_0\_RLAST             Input      1           signal driven by slave-device 
  ==============================  =========  ==========  ============================ 

